package database;

import org.skife.jdbi.v2.sqlobject.SqlUpdate;

public interface ParticipantDao {

    String strCreateParticipantsTable = "Create table if not exists participants ( " +
            "Constraint Username_user Foreign Key (username_User) References User(username)," +
            "Constraint Identifiant_event Foreign Key (identifiant_Event) References Event(identifiant)," +
            "(Username_user, Identifiant_event) primary key)" ;


}