package database;

import json.Event;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

public interface EventDao {

    String strCreateEventsTable = "Create table if not exists events ( " +
            "identifiant int primary key auto_increment," +
            "type text NOT NULL," +
            "description text NOT NULL," +
            "nom text NOT NULL," +
            "Username_user text NOT NULL," +
            "Constraint Username_user Foreign Key (Username_user) References Users(username))" ;

    @SqlUpdate(strCreateEventsTable)
    void createEventTable();

    @SqlUpdate("Insert into events(identifiant, type, description, nom, Username_user) values ( :identifiant, :type, :description, :nom, :Username_user)")
    @RegisterMapperFactory(BeanMapperFactory.class)
    int addEvent(@BindBean Event event);

    @SqlQuery("Select * from events")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Event> getAllEvent();

   @SqlQuery("Select identifiant, type, description, nom")
    Event getEventInfo(@Bind("identifiant") int identifiant);

    @SqlQuery("select * from Event where Username_user=:username")
    List<Event> getEventUser(@Bind("username")String username);
}


