package database;


import json.User;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

public interface UserDao {

    String strCreateUsersTable = "Create table if not exists users ( " +
            "username text primary key," +
            "prenom text NOT NULL," +
            "nom text not null," +
            "mail text NOT NULL," +
            "telephone text NOT NULL," +
            "role text NOT NULL," +
            "password text NOT NULL," +
            "enabled integer default 1 );";


    @SqlUpdate(strCreateUsersTable)
    void createUserTable();

    @SqlUpdate("Insert into users(username, prenom, nom, mail, telephone, password, role) " +
            "values ( :username, :prenom, :nom, :mail, :telephone, :password, 'ROLE_USER')")
    @RegisterMapperFactory(BeanMapperFactory.class)
    int addUser(@BindBean User user);

    @SqlQuery("Select * from users")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<User> getAllUser();

    @SqlQuery("Select username, prenom, nom, mail, telephone, enabled, password, role from users where username = :username")
    User getUserInfo(@Bind("username") String username);
}