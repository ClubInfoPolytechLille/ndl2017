package database;

import json.Vehicule;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

public interface VehiculeDao {

    String strCreateVehiculeTable = "Create table if not exists vehicule ( " +
            "plaque text primary key," +
            "type_typeVehicule text NOT NULL," +
            "modele text not null," +
            "marque text not null," +
            "identifiant_user text not null," +
            "Constraint identifiant_user Foreign Key (identifiant_user) References Users(username));";

    @SqlUpdate(strCreateVehiculeTable)
    int createVehiculeTable();

    @SqlUpdate("Insert into vehicule(plaque, type_typeVehicule, modele, marque, identifiant_user) " +
            "values ( :plaque, :type_typeVehicule, :modele, :marque :identifiant_user)")
    @RegisterMapperFactory(BeanMapperFactory.class)
    int addVehicule(@BindBean Vehicule vehicule);

    @SqlQuery("Select * from vehicule")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Vehicule> getAllVehicules();

    @SqlQuery("Select plaque, type_typeVehicule, modele, marque, identifiant_user from Vehicule where plaque = :plaque")
    Vehicule getVehiculeInfo(@Bind("plaque") String plaque);

    @SqlQuery("SELECT * FROM Vehicule WHERE identifiant_user=:username")
    List<Vehicule> getUserVehicules(@Bind("username") String username);

}
