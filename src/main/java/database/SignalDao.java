package database;

import json.Signal;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

public interface SignalDao {

    String strCreateSignalsTable = "Create table if not exists signal ( " +
            "identifiant int primary key auto_increment," +
            "nom text NOT NULL," +
            "PositionX int NOT NULL," +
            "PositionY int NOT NULL," +
            "username_User text not null," +
            "nom_TypeSignal text not null," +
            "Constraint Username_user_signal Foreign Key (username_User) References Users(username)," +
            "Constraint Nom_typesignal_signal Foreign Key (nom_TypeSignal) References TypeSignal(nom))" ;

    @SqlUpdate(strCreateSignalsTable)
    void createSignalTable();

    @SqlUpdate("Insert into signal(identifiant, nom, PositionX, PositionY) " +
            "values ( :identifiant, :nom, :PositionX, :PosisitionY)")

    @RegisterMapperFactory(BeanMapperFactory.class)
    int addSignal(@BindBean Signal signal);

    @SqlQuery("Select * from signal")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Signal> getAllSignal();

    @SqlQuery("Select identifiant, nom, PositionX, PositionY")
    Signal getSignalInfo(@Bind("identifiant") int identifiant);
}
