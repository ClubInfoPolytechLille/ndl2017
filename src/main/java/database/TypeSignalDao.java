package database;

import json.TypeSignal;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

public interface TypeSignalDao {
    String strCreateTypeSignalTable = "Create table if not exists typeSignal ( " +
            "nom text primary key);";

    @SqlUpdate(strCreateTypeSignalTable)
    void createTypeSignalTable();

    @SqlUpdate("Insert into nom " +
            "values (:nom)")
    @RegisterMapperFactory(BeanMapperFactory.class)
    int addTypeSignal(@BindBean TypeSignal typeSignalDao);

    @SqlQuery("Select * from vehicule")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<TypeSignal> getAllTypesSignal();

    @SqlQuery("Select nom from typeSignal where nom = :nom")
    TypeSignal getTypeSignalInfo(@Bind("typeSignal") String typeSignal);
}