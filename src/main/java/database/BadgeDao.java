package database;

import json.Badge;
import json.User;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

public interface BadgeDao {


    String strCreateBadgesTable = "Create table if not exists badges ( " +
            "nom text primary key," +
            "type text NOT NULL," +
            "description text not null," +
            "enabled integer default 1 );";


    @SqlUpdate(strCreateBadgesTable)
    void createBadgesTable();

    @SqlUpdate("Insert into badges(nom, type, description) " +
            "values ( :nom, :type, :description)")
    @RegisterMapperFactory(BeanMapperFactory.class)
    int addBadge(@BindBean Badge badge);

    @SqlQuery("Select * from badges")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Badge> getAllBadge();

    @SqlQuery("Select nom, type, description from badges where nom = :nom")
    Badge getBadgeInfo(@Bind("nom") String nom);
}

