package database;

import json.TypeVehicule;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;


public interface TypeVehiculeDao {
    String strCreateTypeVehiculeTable = "Create table if not exists typeVehicule ( " +
            "typevehicule text primary key);";

    @SqlUpdate(strCreateTypeVehiculeTable )
    void createTypeVehiculeTable();

    @SqlUpdate("Insert into typeVehicule(type) " +
            "values ( :typeVehicule")
    @RegisterMapperFactory(BeanMapperFactory.class)
    int addTypeVehicule(@BindBean TypeVehicule typeVehicule);

    @SqlQuery("Select * from typeVehicule")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<TypeVehicule> getAllTypeVehicule();

    @SqlQuery("Select typevehicule from TypeVehicule where typeVehicule = :typeVehicule")
    TypeVehicule getTypeVehiculeInfo(@Bind("TypeVehicule") String name);
}
