package rest;


import database.TypeSignalDao;
import json.TypeSignal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;


@RestController
@RequestMapping(value = "/typesignal")
public class TypeSignalRest {

    private TypeSignalDao dao = Application.getDbi().open(TypeSignalDao.class);

    public TypeSignalRest() throws SQLException {
     //   if (!Application.tableExist("typeSignal")) {

            dao.createTypeSignalTable();
       // }
    }

    //CARE THIS IS FOR TESTING
    @RequestMapping(method = RequestMethod.GET)
    public List<TypeSignal> getAllTypesSignal() {
        return dao.getAllTypesSignal();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{nom}")
    public ResponseEntity getTypeSignalInfo(@RequestParam("nom") String nom) {
        System.out.println("Get TypeSignal Info");
        TypeSignal result = dao.getTypeSignalInfo(nom);
        if (result == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.ok().body(result);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity addUseraddTypeSignal(@RequestBody TypeSignal typeSignal) {
        System.out.println("Post TypeSignal");
        int result = dao.addTypeSignal(typeSignal);
        if (result == 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }



}