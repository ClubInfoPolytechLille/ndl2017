package rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HttpErrorHandler {

    String path = "/src/main/ressources/static/";
    @RequestMapping(value= "/error.html")
    public String error404(){
        System.out.println("custom error handler");
        return path;
    }
}