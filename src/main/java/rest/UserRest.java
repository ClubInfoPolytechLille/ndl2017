package rest;


import database.UserDao;
import json.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.sql.SQLException;
import java.util.List;


@RestController
@RequestMapping(value = "/user")
public class UserRest {

    private UserDao dao = Application.getDbi().open(UserDao.class);

    public UserRest() throws SQLException {
    }

    //CARE THIS IS FOR TESTING
    @RequestMapping(method = RequestMethod.GET)
    public List<User> getAllUser() {
        return dao.getAllUser();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{username}")
    public ResponseEntity getUserInfo(@PathParam("username") String username) {
        System.out.println("Get User Info");
        User result = dao.getUserInfo(username);
        if (result == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.ok().body(result);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity addUser(@RequestBody User user) {
        System.out.println("Post User");
        int result = dao.addUser(user);
        if (result == 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }



}