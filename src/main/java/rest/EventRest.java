package rest;


import database.EventDao;
import database.UserDao;
import json.Event;
import json.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.sql.SQLException;
import java.util.List;


@RestController
@RequestMapping(value = "/event")
public class EventRest {

    private EventDao dao = Application.getDbi().open(EventDao.class);

    public EventRest() throws SQLException {
       // if (!Application.tableExist("Events")) {
            dao.createEventTable();
            //dao.addEvent(new Event(0, "type", "Event", "Alert", "badetitou"));
        //}
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Event> getAllEvent(){ return dao.getAllEvent();}

    @RequestMapping(method = RequestMethod.GET, value = "/{identifiant}")
    public ResponseEntity getEventInfo(@PathParam("identifiant") int identifiant) {
        System.out.println("Get Event Info");
        Event result = dao.getEventInfo(identifiant);
        if (result == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.ok().body(result);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity addEvent(@RequestBody Event event) {
        System.out.println("Post Event");
        int result = dao.addEvent(event);
        if (result == 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.ok().body(null);
    }
}
