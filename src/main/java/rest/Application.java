package rest;


import database.TypeSignalDao;
import database.UserDao;
import json.User;
import org.h2.jdbcx.JdbcConnectionPool;
import org.skife.jdbi.v2.DBI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.sql.DataSource;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;


@SpringBootApplication
@EnableAutoConfiguration
public class Application extends WebMvcConfigurerAdapter {

    private static DBI dbi = null;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    private static void initBase(){
        UserDao userDao = Application.getDbi().open(UserDao.class);
        userDao.createUserTable();
        userDao.addUser(new User("badetitou", "mdp", "Benoit", "Verhaeghe", "benoit.ver@wanadoo.com", "0600000000", "ROLE_ADMIN"));
    }



    @Bean
    public DataSource getDataSource() {
        return JdbcConnectionPool.create("jdbc:h2:mem:test", "username", "password");
    }

    @Autowired
    public void setDbi(DataSource dataSource) {
        dbi = new DBI(dataSource);
        initBase();
    }

    static DBI getDbi() {
        return dbi;
    }

}
