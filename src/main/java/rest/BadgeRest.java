package rest;


import database.BadgeDao;
import json.Badge;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;


@RestController
@RequestMapping(value = "/badge")
public class BadgeRest {

    private BadgeDao dao = Application.getDbi().open(BadgeDao.class);

    public BadgeRest() throws SQLException {
       // if (!Application.tableExist("badges")) {
            dao.createBadgesTable();
            dao.addBadge(new Badge("Sauveur", "Action", "Avoir donné les premiers soins."));

       // }
    }

    //CARE THIS IS FOR TESTING
    @RequestMapping(method = RequestMethod.GET)
    public List<Badge> getAllBadge() {
        return dao.getAllBadge();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{nom}")
    public ResponseEntity getBadgeInfo(@RequestParam("nom") String nom) {
        System.out.println("Get Badge Info");
        Badge result = dao.getBadgeInfo(nom);
        if (result == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.ok().body(result);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity addUser(@RequestBody Badge badge) {
        System.out.println("Post Badge");
        int result = dao.addBadge(badge);
        if (result == 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }



}