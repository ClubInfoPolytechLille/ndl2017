package rest;

import database.VehiculeDao;
import json.Vehicule;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping(value = "/vehicule")
public class VehiculeRest {


    private VehiculeDao dao = Application.getDbi().open(VehiculeDao.class);


    public VehiculeRest() throws SQLException {
           dao.createVehiculeTable();
          //  dao.addVehicule(new Vehicule("0", "type", "207", "Renault" , "iduser1"));

    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Vehicule> getAllVehicules(){ return dao.getAllVehicules();}



    @RequestMapping(method = RequestMethod.GET, value = "/{username}")
    public List<Vehicule> getVehiculeInfo(@PathParam("username") String username) {
        return dao.getUserVehicules(username);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity addVehicule(@RequestBody Vehicule vehicle) {
        System.out.println("Post Vehicle");
        int result = dao.addVehicule(vehicle);
        if (result == 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.ok().body(null);
    }
}
