package rest;

import database.TypeSignalDao;
import database.TypeVehiculeDao;
import json.TypeSignal;
import json.TypeVehicule;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;


@RestController
@RequestMapping(value = "/typevehicule")
public class TypeVehiculeRest {


    private TypeVehiculeDao dao = Application.getDbi().open(TypeVehiculeDao.class);


    public TypeVehiculeRest() throws SQLException {
           // dao.createTypeVehiculeTable();
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<TypeVehicule> getAllTypeVehicule() {
        return dao.getAllTypeVehicule();
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity addTypeVehicule(@RequestBody TypeVehicule user) {
        System.out.println("Post type vehicule");
        int result = dao.addTypeVehicule(user);
        if (result == 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/{typeVehicule}")
    public ResponseEntity getTypeVehiculeInfo(@RequestParam("typevehicule") String typevehicule) {
        System.out.println("Get TypeVehicule Info");
        TypeVehicule result = dao.getTypeVehiculeInfo(typevehicule);
        if (result == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.ok().body(result);
    }
}
