package rest;

import database.SignalDao;
import database.TypeSignalDao;
import json.Signal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;


@RestController
@RequestMapping(value = "/signal")

public class SignalRest {

    private SignalDao dao = Application.getDbi().open(SignalDao.class);

    public SignalRest() throws SQLException {
       // if (!Application.tableExist("signal")) {
            //TypeSignalDao typeSignalDao = Application.getDbi().open(TypeSignalDao.class);
        //}
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Signal> getAllSignal() {
        return dao.getAllSignal();
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity addSignal(@RequestBody Signal user) {
        System.out.println("Post Signal");
        int result = dao.addSignal(user);
        if (result == 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/{identifiant}")
    public ResponseEntity getSignalrInfo(@RequestParam("identifiant") int identifiant) {
        System.out.println("Get Signal Info");
        Signal result = dao.getSignalInfo(identifiant);
        if (result == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.ok().body(result);
    }
}
