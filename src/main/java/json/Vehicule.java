package json;

import java.io.Serializable;

public class Vehicule implements Serializable {

    private String type_typeVehicule;
    private String modele;
    private String marque;
    private String plaque;
    private String identifiant_user;

    public Vehicule()
    {

    }

    public Vehicule(String type_typeVehicule, String modele, String marque, String plaque, String identifiant_user) {
        this.type_typeVehicule = type_typeVehicule;
        this.modele = modele;
        this.marque = marque;
        this.plaque = plaque;
        this.identifiant_user = identifiant_user;
    }

    public String getType_typeVehicule() {
        return type_typeVehicule;
    }

    public void setType_typeVehicule(String type_typeVehicule) {
        this.type_typeVehicule = type_typeVehicule;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getPlaque() {
        return plaque;
    }

    public void setPlaque(String plaque) {
        this.plaque = plaque;
    }

    public String getIdentifiant_user() {
        return identifiant_user;
    }

    public void setIdentifiant_user(String identifiant_user) {
        this.identifiant_user = identifiant_user;
    }
}
