package json;

import java.io.Serializable;

public class User implements Serializable {

    private String username;
    private String prenom;
    private String nom;
    private String mail;
    private String telephone;
    private String password;
    private String role;
    private int enabled;


    public User() {
    }

    public User(String username, String password, String prenom, String nom, String mail, String telephone, String role) {
        this.username = username;
        this.password = password;
        this.prenom = prenom;
        this.nom = nom;
        this.mail = mail;
        this.telephone = telephone;
        this.role = role;

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPrenom(){ return prenom; }

    public void setPrenom( String prenom) { this.prenom =  prenom; }

    public String getNom(){ return nom; }

    public void setNom( String nom) { this.nom =  nom; }

    public String getMail(){ return mail; }

    public void setMail( String mail) { this.mail =  mail; }

    public String getTelephone(){ return telephone; }

    public void setTelephone( String telephone) { this.telephone =  telephone; }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
