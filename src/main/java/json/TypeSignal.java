package json;

import java.io.Serializable;

public class TypeSignal implements Serializable{

    private String nom;


    public TypeSignal() {
    }

    public TypeSignal(String nom) {
        this.nom = nom;

    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

}