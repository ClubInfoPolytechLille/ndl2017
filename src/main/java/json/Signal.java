package json;

import java.io.Serializable;

public class Signal {

    private int identifiant;
    private String nom;
    private int positionX;
    private int positionY;
    private int enabled;
    private String Username_user;
    private String Nom_typesignal;

    public Signal() {
    }

    public Signal(int identifiant, String nom, int positionX, int positionY, int enabled, String username_user, String nom_typesignal) {
        this.identifiant = identifiant;
        this.nom = nom;
        this.positionX = positionX;
        this.positionY = positionY;
        this.enabled = enabled;
        Username_user = username_user;
        Nom_typesignal = nom_typesignal;
    }

    public int getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(int identifiant) {
        this.identifiant = identifiant;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public String getUsername_user() {
        return Username_user;
    }

    public void setUsername_user(String username_user) {
        Username_user = username_user;
    }

    public String getNom_typesignal() {
        return Nom_typesignal;
    }

    public void setNom_typesignal(String nom_typesignal) {
        Nom_typesignal = nom_typesignal;
    }
}

