package json;

import java.io.Serializable;

public class TypeVehicule implements Serializable {

    private String typeVehicule;

    public TypeVehicule() {
    }

    public TypeVehicule(String typeVehicule) {
        this.typeVehicule = typeVehicule;
    }

    public String getTypeVehicule() {
        return typeVehicule;
    }

    public void setTypeVehicule(String typeVehicule) {
        this.typeVehicule = typeVehicule;
    }
}
