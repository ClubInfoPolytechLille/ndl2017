package json;

import java.io.Serializable;

public class Badge implements Serializable{
    private String nom;
    private String type;
    private String description;


    public Badge() {
    }

    public Badge(String nom, String type, String descrption) {
        this.nom = nom;
        this.type = type;
        this.description = descrption;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
