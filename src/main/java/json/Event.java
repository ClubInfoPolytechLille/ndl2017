package json;

import java.io.Serializable;

public class Event implements  Serializable{


    private int identifiant;
    private String type;
    private String description;
    private String nom;
    private String Username_user;


    public Event() {
    }

    public Event(int identifiant, String type, String description, String nom, String Username_user) {

        this.identifiant = identifiant;
        this.type = type;
        this.description = description;
        this.nom = nom;
        this.Username_user = Username_user;

    }

    public String getUsername_user() {
        return Username_user;
    }

    public void setUsername_user(String Username_user) { this.Username_user = Username_user; }

    public int getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(int identifiant) {
        this.identifiant = identifiant;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


}

