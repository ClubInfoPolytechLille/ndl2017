$(document).ready(function() {


    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/username',
        error: function () {
            $('#info').html('<p>An error has occurred</p>');
        },
        success: function (data) {

          completeEvent(data);
          completeVehicule(data);
        }
    });

    function completeEvent(username){
        $.ajax({
            type: 'GET',
            url: 'http://localhost:8080/event/'+username,
            error: function () {
                $('#info').html('<p>An error has occurred</p>');
            },
            success: function (data) {
                $.each(data, function (index, element) {
                    $('#event').append('<div>'+ element.nom +'</div>');
                });
            },
            error: function (e) {
                alert("ERROR : " + e);
            }
        });
    }

    function completeVehicule(username){
        $.ajax({
            type: 'GET',
            url: 'http://localhost:8080/vehicule/'+username,
            error: function () {
                $('#info').html('<p>An error has occurred</p>');
            },
            success: function (data) {
                $.each(data, function (index, element) {
                    $('#vehicule').append('<div>'+ element.plaque +'</div>');
                });
            },
            error: function (e) {
                alert("ERROR : " + e);
            }
        });
    }

});



