$(document).ready(function() {

    $.ajax({
        type: 'GET',
        url: 'http://127.0.0.1:8080/Vehicule',
        data: {
            format: 'json'
        },
        error: function () {
            $('#info').html('<p>An error has occurred</p>');
        },
        dataType: 'json',
        success: function (data) {
            $.each(data, function (index, element) {
                $('#vehicules').append('<tr><td>'+ element.marque +'</td><td>' + element.modele+ '</td><td>\' + element.plaque+ \'</td></tr>');
            });
        }
    });

});
