$(document).ready(function() {

    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/username',
        data: {
            format: 'json'
        },
        error: function () {
            $('#info').html('<p>An error has occurred</p>');
        },
        dataType: 'json',
        success: function (data) {
            updateEvent(data);
        }
    });

});

function updateEvent(username) {
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/event/' + username,
        data: {
            format: 'json'
        },
        error: function () {
            $('#info').html('<p>An error has occurred</p>');
        },
        dataType: 'json',
        success: function (data) {
            $.each(data, function (index, element) {
                $('#userEvent').append('<tr><td>'+ element.nom +'</td><td>' + element.description + '</td></tr>');
            });
        }
    });

}